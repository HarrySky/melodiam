import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'
import * as React from 'react'
import 'ui/css/CurrentSong.css';

interface State {
  isAvailable: boolean;
  progress: number;
  albumName?: string;
  albumURL?: string;
  albumImageURL?: string;
  artist?: string;
  artistURL?: string;
  context?: string;
  contextURL?: string;
  trackName?: string;
  trackURL?: string;
}

// tslint:disable-next-line: no-default-export
export default class CurrentSong extends React.PureComponent<{}, State> {
  state: State = {
    isAvailable: false,
    progress: 0,
  }

  private timer = -1;

  render() {
    const currentSong = this.state;
    if (!currentSong.isAvailable) {
      currentSong.progress = 0
      currentSong.context = "No Context"
      currentSong.contextURL = "#"
      currentSong.albumURL = "#"
      currentSong.albumName = "Right Now"
      currentSong.albumImageURL = "/favicon.ico"
      currentSong.trackURL = "#"
      currentSong.trackName = "Is Playing"
      currentSong.artistURL = "#"
      currentSong.artist = "No Track"
    }
    
    return (
      <div className="song-container">
        <Card>
          <div className="song-info">
            <a href={currentSong.albumURL}>
              <CardMedia
                className="song-album"
                component="img"
                image={currentSong.albumImageURL}
                title={currentSong.albumName} />
            </a>
            <div className="song-details-container">
              <CardContent className="song-details">
                <Typography variant="subtitle2" color="secondary" noWrap={true}>
                  <a href={currentSong.contextURL}>{currentSong.context}</a>
                </Typography>
                <Typography variant="subtitle1" noWrap={true}>
                  <a href={currentSong.artistURL}>{currentSong.artist}</a>
                </Typography>
                <Typography variant="subtitle2" color="textSecondary" noWrap={true}>
                  <a href={currentSong.trackURL}>{currentSong.trackName}</a>
                </Typography>
              </CardContent>
            </div>
          </div>
          <LinearProgress color="secondary" variant="determinate" value={currentSong.progress} />
        </Card>
      </div>
    )
  }

  componentDidMount() {
    this.getSong()
    setInterval(this.getSong, 5000);
  }

  private updateProgress = (oneSecondStep:number) => {
    const progress = this.state.progress + oneSecondStep;
    this.setState({progress});
  }

  private getSong = () => {
    fetch('/api/current_song', {
      method: 'GET'
    }).then(currentSongResponse => {
      if (currentSongResponse.status !== 200) { return "null" }

      return currentSongResponse.text()
    }).then(currentSongResponseText => {
      window.clearInterval(this.timer)

      if (currentSongResponseText === "null") {
        this.setState({isAvailable: false, progress: 0});
      } else {
        let song;
        try {
          song = JSON.parse(currentSongResponseText)
        } catch {
          this.setState({isAvailable: false, progress: 0});
          return;
        }

        // Update only if track exist
        if (song.item) {
          const progress = (song.progress_ms / song.item.duration_ms) * 100;
          const isAvailable = true;
          const context = song.context ? song.context.name ? song.context.name : "Fetching Context..." : "My Liked Songs"
          const contextURL = song.context ? song.context.external_urls.spotify : "#";
          const albumImageURL = song.item.album.images[1].url
          const albumName = song.item.album.name
          const albumURL = song.item.album.external_urls.spotify
          const artist = song.item.artists[0].name
          const artistURL = song.item.artists[0].external_urls.spotify
          const trackName = song.item.name
          const trackURL = song.item.external_urls.spotify

          // Only update progress every second if song not paused
          if (!song.item.paused) {
            const oneSecondStep = (1000/song.item.duration_ms) * 100
            this.timer = window.setInterval(this.updateProgress, 1000, oneSecondStep)
          }

          this.setState({
            isAvailable,
            progress,
            context,
            contextURL,
            albumImageURL,
            albumName,
            albumURL,
            artist,
            artistURL,
            trackName,
            trackURL
          });
        }
      }
    })
  }
}
