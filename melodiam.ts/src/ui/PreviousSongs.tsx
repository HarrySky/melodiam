import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import Popover from '@material-ui/core/Popover'
import Typography from '@material-ui/core/Typography'
import ListIcon from '@material-ui/icons/List'
import * as React from 'react'
import 'ui/css/PreviousSongs.css';

interface PreviousSong {
  isAvailable: boolean;
  artist?: string;
  artistURL?: string;
  trackName?: string;
  trackURL?: string;
}

interface State {
  previousSongs: PreviousSong[];
  previousSongsAnchorEl?: Element;
}

// tslint:disable-next-line: no-default-export
export default class PreviousSongs extends React.PureComponent<{}, State> {
  state: State = {
    // tslint:disable-next-line: ban
    previousSongs: [...Array(5)].map((_, i) => ({isAvailable: false})),
    previousSongsAnchorEl: undefined
  }

  render() {
    const previousSongs = []
    const openPreviousSongs = Boolean(this.state.previousSongsAnchorEl)
    if (openPreviousSongs) {
      for (let i = 0; i < 5; i++) {
        if (i > 0) {
          previousSongs.push(<Divider />)
        }

        if (!this.state.previousSongs[i].isAvailable) {
          previousSongs.push(<b>-</b>)
          continue
        }

        previousSongs.push(
          <Typography variant="subtitle1" align='center' noWrap={true}>
            <a href={this.state.previousSongs[i].artistURL}>{this.state.previousSongs[i].artist}</a>
          </Typography>
        )
        previousSongs.push(
          <Typography variant="subtitle2" color="textSecondary" align='center' noWrap={true}>
            <a href={this.state.previousSongs[i].trackURL}>{this.state.previousSongs[i].trackName}</a>
          </Typography>
        )
      }
    }

    return (
      <div>
        <Button onClick={this.openPreviousSongs} variant="contained" color="primary" size="small" fullWidth={true}>
          <ListIcon className="left-icon" />
          Previous 5 Songs
        </Button>
        <Popover 
          onClose={this.closePreviousSongs}
          open={openPreviousSongs}
          anchorEl={this.state.previousSongsAnchorEl}
          anchorOrigin={{
            horizontal: 'center',
            vertical: 'top'
          }}
          transformOrigin={{
            horizontal: 'center',
            vertical: 'bottom'
          }}
        >
          <div className="prev-songs">
            {previousSongs}
          </div>
        </Popover>
      </div>
    )
  }

  componentDidMount() {
    this.getSongs()
    setInterval(this.getSongs, 5000);
  }

  private closePreviousSongs = () => this.setState({previousSongsAnchorEl: undefined});

  private openPreviousSongs = (event: React.MouseEvent<HTMLElement>) => this.setState({previousSongsAnchorEl: event.currentTarget as HTMLElement});

  private getSongs = () => {
    fetch('/api/history', {
      method: 'GET'
    }).then(previousSongsResponse => {
      if (previousSongsResponse.status !== 200) { return null }

      return previousSongsResponse.json()
    }).then(previousSongsResponseArray => {
      if (previousSongsResponseArray === null) { return }

      // tslint:disable-next-line: ban
      const previousSongs: PreviousSong[] = [...Array(5)].map((_, i) => ({isAvailable: false}));
      for (let i = 0; i < 5; i++) {
        const previousSong = previousSongsResponseArray[i]
        if (!previousSong) {
          previousSongs[i].isAvailable = false
          continue
        }

        previousSongs[i].isAvailable = true;
        previousSongs[i].artist = previousSong.artists[0].name;
        previousSongs[i].artistURL = previousSong.artists[0].external_urls.spotify;
        previousSongs[i].trackName = previousSong.name;
        previousSongs[i].trackURL = previousSong.external_urls.spotify;
      }

      this.setState({previousSongs})
    })
  }
}
