import Avatar from '@material-ui/core/Avatar'
import Chip from '@material-ui/core/Chip'
import Typography from '@material-ui/core/Typography'
import * as React from 'react'
import 'ui/css/User.css'

interface State {
  id?: string,
  url?: string,
  imageURL?: string
}

// tslint:disable-next-line: no-default-export
export default class User extends React.PureComponent<{}, State> {
  state: State = {
    id: undefined,
    url: undefined,
    imageURL: undefined,
  }

  render() {
    const avatar = this.state.imageURL ? this.state.imageURL : "/unknown.png"
    const username = this.state.id ? this.state.id : "Login, Please!"

    return (
      <div className="user-container">
        <Chip
          className="user"
          color="primary"
          variant="outlined"
          avatar={<Avatar src={avatar} alt="Avatar"/>}
          label={<Typography className="username" color="primary" variant="subtitle2" noWrap={true}>{username}</Typography>}
          onClick={this.openProfile}
        />
      </div>
    )
  }

  componentDidMount() {
    this.getUser()
    setInterval(this.getUser, 5000);
  }

  private openProfile = () => {
    const profileURL = this.state.url ? this.state.url : "#"
    window.open(profileURL, "_self")
  }

  private getUser = () => {
    fetch('/api/user', {
      method: 'GET'
    }).then(usernameResponse => {
      if (usernameResponse.status !== 200) { return "null" }

      return usernameResponse.text()
    }).then(usernameResponseText => {
      if (usernameResponseText === "null") {
        this.setState({id: "", imageURL: "/unknown.png", url: "#"});
      } else {
        let user
        try {
          user = JSON.parse(usernameResponseText)
        } catch {
          this.setState({id: "", imageURL: "/unknown.png", url: "#"});
          return;
        }

        const id = user.id
        const url = user.external_urls.spotify
        const image = user.images.length > 0 ? user.images[0] : null;
        const imageURL = image ? image.url : "/unknown.png";
        this.setState({id, imageURL, url});
      }
    })
  }
}
