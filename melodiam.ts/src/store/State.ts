import { ScreenData } from 'DataTypes';
import { getScreenData } from './utils';

interface ScreenState {
  screen: ScreenData;
}

export interface AppState
  extends ScreenState {}

export const INITIAL_STATE: AppState = {
  screen: getScreenData(),
};
