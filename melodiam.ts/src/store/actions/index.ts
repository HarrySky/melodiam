import { ScreenData } from 'DataTypes';

export enum Actions {
  SCREEN_UPDATE = 'SCREEN_UPDATE',
}

export interface ScreenAction {
  type: typeof Actions.SCREEN_UPDATE;
  screen: ScreenData;
}
