import { ScreenData } from 'DataTypes';

export function update<S>(state: S, newState: Partial<S>): S {
  return Object.assign({}, state, newState);
}

export function getScreenData(): ScreenData {
  const width = window.innerWidth;
  const height = window.innerHeight;
  const isPortrait = window.matchMedia('(orientation: portrait)').matches;
  const isMobile = isPortrait ? width < 500 : height < 500;

  return { width, height, isPortrait, isMobile };
}
