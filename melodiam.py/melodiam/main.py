import logging
from typing import Any, List, Optional

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from httpx import AsyncClient
from starlette.applications import Starlette
from starlette.config import Config
from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request as StarletteRequest
from starlette.responses import PlainTextResponse, RedirectResponse

from tekore import Spotify
from tekore.auth.expiring import Credentials, HTTPError, Token
from tekore.model.currently_playing import CurrentlyPlaying
from tekore.model.track import FullTrack
from tekore.model.user import PrivateUser
from tekore.model.context import ContextType, Context
from tekore.sender import AsyncPersistentSender

CONFIG = Config(env_file="melodiam.env")
# Is debug mode turned on
DEBUG = CONFIG.get("DEBUG", cast=bool, default=False)
# Secret key for encrypting sessions
SECRET_KEY = CONFIG.get("SECRET_KEY", default="change-me")
# Application's credentials needed for OAuth2.0
CLIENT_ID = CONFIG.get("CLIENT_ID", default=None)
CLIENT_SECRET = CONFIG.get("CLIENT_SECRET", default=None)
REDIRECT_URI = CONFIG.get("REDIRECT_URI", default=None)
# Interval in seconds to update currently playing song
SONG_UPDATE_INTERVAL = CONFIG.get("SONG_UPDATE_INTERVAL", cast=int, default=5)
# How many seconds we wait for response from API
API_RESPONSE_TIMEOUT = CONFIG.get("API_REQUESTS_TIMEOUT", cast=int, default=1)
# Secret password for authentication as main user
MAIN_USER_SECRET = CONFIG.get("MAIN_USER_SECRET", default="change-me")

# Remove logs about successfully executed jobs (if not debug)
logging.getLogger("apscheduler").setLevel(logging.DEBUG if DEBUG else logging.WARNING)

MELODIAM_LOGGER = logging.getLogger("melodiam")
MELODIAM_LOGGER.setLevel(logging.DEBUG if DEBUG else logging.INFO)


def same_contexts(old: Any, new: Any) -> bool:
    if old is None or new is None:
        return old == new

    external_urls = old["external_urls"] if type(old) is dict else old.external_urls
    return external_urls["spotify"] == new.external_urls["spotify"]
    

class Melodiam(Starlette):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        # How many seconds current song was listened for
        self.listened_for = 0

        self._credentials: Credentials = None
        self._user_token: Token = None

        # Scheduler for interval jobs making requests to API
        self._scheduler: AsyncIOScheduler = None

        # API that will be used for making request as main user
        self._user_api: Spotify = None
        # API that will be used for making requests as Listen Along users
        self._listen_along_api: Spotify = None

        # Main user data
        self.user: Optional[PrivateUser] = None
        self.current_song: Optional[CurrentlyPlaying] = None
        self.history: List[Optional[FullTrack]] = [None for _ in range(5)]

    @property
    def credentials(self) -> Credentials:
        """
        Credentials used for OAuth2.0 authentication process
        """
        if self._credentials is None:
            self._credentials = Credentials(
                CLIENT_ID,
                CLIENT_SECRET,
                REDIRECT_URI,
                sender=AsyncPersistentSender(
                    AsyncClient(http2=True, timeout=API_RESPONSE_TIMEOUT)
                ),
            )

        return self._credentials

    @property
    def user_token(self) -> Token:
        """
        Token used for making request as main user
        """
        if self.user_api.token is None:
            raise Exception("No token exist, please log in!")

        return self.user_api.token

    @user_token.setter
    def user_token(self, value: Token) -> None:
        self.user_api.token = value

    @property
    def user_api(self) -> Spotify:
        if self._user_api is None:
            self._user_api = Spotify(
                sender=AsyncPersistentSender(
                    AsyncClient(http2=True, timeout=API_RESPONSE_TIMEOUT)
                ),
            )

        return self._user_api

    @property
    def listen_along_api(self) -> Spotify:
        if self._listen_along_api is None:
            self._listen_along_api = Spotify(
                sender=AsyncPersistentSender(
                    AsyncClient(http2=True, timeout=API_RESPONSE_TIMEOUT)
                ),
            )

        return self._listen_along_api

    @property
    def scheduler(self) -> AsyncIOScheduler:
        if self._scheduler is None:
            self._scheduler = AsyncIOScheduler(timezone="Europe/Tallinn")
            self._scheduler.start()

        return self._scheduler

    @property
    def current_track(self) -> Optional[FullTrack]:
        return None if self.current_song is None else self.current_song.item

    @property
    def user_json(self) -> str:
        return "null" if self.user is None else str(self.user)

    @property
    def current_song_json(self) -> str:
        return "null" if self.current_song is None else str(self.current_song)

    @property
    def history_json(self) -> str:
        return f"""[{
            ','.join(
                ['null' if track is None else str(track) for track in self.history]
            )
        }]"""

    async def refresh_token(self) -> None:
        # Refresh tokens if they expire in less than 5 minutes
        if self.user_token.expires_in < 300:
            self.user_token = await self.credentials.refresh(self.user_token)
            MELODIAM_LOGGER.info("User token successfully refreshed!")

    async def _set_context(self) -> None:
        current_context = self.current_song.context
        if not current_context:
            return

        if current_context.type == ContextType.artist:
            self.current_song.context = self.current_song.item.artists[0]        
        elif current_context.type == ContextType.album:
            self.current_song.context = self.current_song.item.album
        else:
            playlist_id = current_context.uri.split(":")[-1]
            self.current_song.context = await self.user_api.playlist(
                playlist_id,
                fields="name,external_urls"
            )

    async def get_current_song(self) -> None:
        old_track = self.current_track
        old_context = None if self.current_song is None else self.current_song.context
        new_song = await self.user_api.playback_currently_playing()
        new_track: Optional[FullTrack] = None if new_song is None else new_song.item

        self.current_song = new_song

        # Something is playing right now
        if new_track is not None:
            # Nothing was playing before
            # [change to other song from no song happened]
            if old_track is None:
                self.listened_for = 0
                await self._set_context()
            # Same track was playing before
            # [update of current song needed]
            elif new_track.id == old_track.id:
                self.listened_for += SONG_UPDATE_INTERVAL * 1000
                new_song.context = old_context
            # Different track was playing before
            # [change to other song from some song happened]
            else:
                # Deal with context
                if same_contexts(old_context, new_song.context):
                    new_song.context = old_context
                else:
                    await self._set_context()

                # Different track was playing before for at least 30 seconds
                # [history rotation needed]
                if self.listened_for >= 30 * 1000:
                    self.listened_for = 0
                    self.history = self.history[-1:] + self.history[:-1]
                    self.history[0] = old_track
        # Nothing is playing right now, but was playing before
        # [change to no song from some song happened]
        elif old_track is not None:
            # Previous track played for at least 30 seconds
            # [history rotation needed]
            if self.listened_for >= 30 * 1000:
                self.listened_for = 0
                self.history = self.history[-1:] + self.history[:-1]
                self.history[0] = old_track

    async def add_listen_along_user(self, user: PrivateUser, token: Token) -> None:
        pass


MELODIAM = Melodiam(debug=DEBUG)
MELODIAM.add_middleware(
    SessionMiddleware,
    secret_key=SECRET_KEY,
    same_site="lax",
    https_only=not DEBUG,
)

@MELODIAM.route("/status")
async def status(request: StarletteRequest) -> PlainTextResponse:
    return PlainTextResponse("OK")


@MELODIAM.route("/auth")
async def auth(request: StarletteRequest) -> PlainTextResponse:
    secret = request.query_params.get("secret", None)
    if secret == MAIN_USER_SECRET:
        request.session["main_user"] = "1"
        return PlainTextResponse("Welcome, Main User!")

    return PlainTextResponse("Invalid Secret - Access Denied!")


@MELODIAM.route("/login")
async def login(request: StarletteRequest) -> RedirectResponse:
    if request.session.get("main_user") != "1":
        return RedirectResponse("/?error=UNAUTHORIZED")

    return RedirectResponse(
        url=MELODIAM.credentials.user_authorisation_url(
            scope="user-read-currently-playing",
        )
    )


@MELODIAM.route("/login_redirect")
async def login_redirect(request: StarletteRequest) -> RedirectResponse:
    code = request.query_params.get("code", default=None)
    if code is None:
        return RedirectResponse("/?error=NO_CODE")

    main_user = request.session.get("main_user") == "1"
    state = request.query_params.get("state", default=None)
    if not main_user and state != "listen-along":
        return RedirectResponse("/?error=UNAUTHORIZED")

    try:
        token = await MELODIAM.credentials.request_user_token(code)
    except HTTPError:
        MELODIAM_LOGGER.error("OAuth2.0 Failed!", exc_info=1)
        return RedirectResponse("/?error=OAUTH_FAIL")

    if main_user:
        MELODIAM.user_token = token
        user = await MELODIAM.user_api.current_user()
        MELODIAM.user = user

        MELODIAM.scheduler.remove_all_jobs()
        MELODIAM.scheduler.add_job(MELODIAM.refresh_token, "interval", minutes=1)
        MELODIAM.scheduler.add_job(
            MELODIAM.get_current_song, "interval", seconds=SONG_UPDATE_INTERVAL
        )
    else:
        with MELODIAM.listen_along_api.token_as(token):
            user = await MELODIAM.listen_along_api.current_user()

        MELODIAM.add_listen_along_user(user, token)

    request.session["user"] = user.id
    return RedirectResponse("/")


@MELODIAM.route("/user")
async def user(request: StarletteRequest) -> PlainTextResponse:
    return PlainTextResponse(MELODIAM.user_json, media_type="application/json")


@MELODIAM.route("/current_song")
async def current_song(request: StarletteRequest) -> PlainTextResponse:
    return PlainTextResponse(MELODIAM.current_song_json, media_type="application/json")


@MELODIAM.route("/history")
async def history(request: StarletteRequest) -> PlainTextResponse:
    return PlainTextResponse(MELODIAM.history_json, media_type="application/json")

@MELODIAM.route("/listen_along")
async def listen_along(request: StarletteRequest) -> RedirectResponse:
    if request.session.get("main_user") == "1":
        return RedirectResponse("/")

    return RedirectResponse(
        url=MELODIAM.credentials.user_authorisation_url(
            scope="user-read-currently-playing",
            state="listen-along"
        )
    )
