Melodiam
========

**Melodiam** is a backend+frontend solution for presenting data from **Spotify Web API** on webpage in real-time.

See `backend` folder and `frontend` folder for more information.
